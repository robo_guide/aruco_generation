import cv2
import numpy as np
import pyrealsense2 as rs

# markers to detect
dict_enum = cv2.aruco.DICT_4X4_50
marker_dict = cv2.aruco.getPredefinedDictionary(dict_enum)

# start realsense color stream
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
pipeline.start(config)

# start opencv camera
# video = cv2.VideoCapture(0)
while cv2.waitKey(30) == -1:
    # via realsense
    frames = pipeline.wait_for_frames()
    image = frames.get_color_frame()
    image = np.asanyarray(image.get_data())

    # via opencv
    # _, image = video.read()

    # visualize
    corners, ids, _ = cv2.aruco.detectMarkers(image, marker_dict)
    cv2.aruco.drawDetectedMarkers(image, corners, ids)
    cv2.imshow("aruco", image)
