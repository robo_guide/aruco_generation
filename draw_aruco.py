import cv2
import os

# prepare generation
dict_enum = cv2.aruco.DICT_4X4_50
marker_dict = cv2.aruco.getPredefinedDictionary(dict_enum)
if not os.path.exists("marker_images"):
    os.makedirs("marker_images")
# generate markers
for marker_id in range(0, 4):
    image = cv2.aruco.drawMarker(marker_dict, marker_id, 500)
    cv2.imwrite("marker_images/aruco_marker_4x4_{}.png".format(marker_id),
                image)
    cv2.imshow("aruco", image)
    cv2.waitKey(0)
