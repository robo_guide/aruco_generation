# Installation
Uses git-lfs for images so make sure you have it installed!

Use virtualenv to install the requirements.txt via pip:
```
virtualenv venv
pip install -r requirements.txt
```
Note: pyrealsense is only required if a realsense camera is used.